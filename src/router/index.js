import VueRouter from 'vue-router'
import Login from '@/views/Login'
import Home from '@/views/Home'

const routes = [
  {
    path: '/login',
    name: 'Login',
    component: Login,
    meta: {
      showInNavigation: false,
    },
  },
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: {
      showInNavigation: true,
    },
  },
]

const router = new VueRouter({
  routes
})

export default router