import Vue from 'vue'
import App from '@/App.vue'
import VueMeta from 'vue-meta'
import VueRouter from 'vue-router'

import router from '@router/index.js'

Vue.use(VueMeta)
Vue.use(VueRouter)

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
})
  .$mount('#app')