export default {
  name: 'LeftNavigation',
  data() {
    return {
      routes: [],
    }
  },
  beforeMount() {
    this.$data.routes = this.$router.options.routes
      .filter(x => x.meta.showInNavigation)
      .map(x => { return {name: x.name, path: x.path}})
  },
  methods: {
    getPathClasses: function(pageName) {
      return {
        "hover:bg-gray-200": true,
        "hover:text-gray-700": true,
        "px-4": true,
        "bg-gray-600": pageName === this.$route.name,
        "text-gray-100": pageName === this.$route.name,
      }
    }
  },
}