export default {
  name: 'Header',
  data() {
    return {
      topRightHangingEement: '',
    }
  },
  methods: {
    openNotification: function() {
      this.$data.topRightHangingEement = 'notification'
    },
    openMenu: function() {
      this.$data.topRightHangingEement = 'menu'
    }
  },
}