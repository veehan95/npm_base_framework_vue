import LeftNavigation from '@/components/LeftNavigation'
import Header from '@/components/Header'

export default {
  name: 'app',
  components: { LeftNavigation, Header },
  metaInfo: {
    title: 'dafuckin home',
    titleTemplate: '%s | BackEnd',
    link: [
      { rel: "stylesheet", href: "https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css"},
    ],
    script: [
      { src: "https://kit.fontawesome.com/54668b1b35.js", crossorigin: "anonymous"},
    ]
  },
}