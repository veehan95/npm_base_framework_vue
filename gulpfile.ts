import  { series, src, dest, watch } from 'gulp'
import { obj as t2Obj } from 'through2'
import rename from 'gulp-rename'
import change from 'gulp-change'
import clean from 'gulp-clean'
import loganize from 'loganize'

const tp = [
  {
    dir: "./src/pug",
    prop: { ext: "pug", lang: "pug", tag: "template" },
  }, {
    dir: "./src/typescript",
    prop: { ext: "ts", lang: "ts", tag: "script" },
  }, {
    dir: "./src/sass",
    prop: { ext: "sass", lang: "sass", scoped: true, tag: "style" },
  },
]


type ScriptProp = {
  ext: string,
  tag: string,
  lang?: string,
  scoped?: boolean,
}

loganize.info("gulp started")

class GulpVueBuilder {
  constructor(
    private toProcess: {dir: string, prop: ScriptProp}[],
    private destination: string = './_build',
  ) {}

  private dataChunker = (dir: string, prop: ScriptProp, destination?: string): Promise<void> => {
    return new Promise(res => {
      src(dir, {allowEmpty: true})
        .pipe(change(function (content) {
          return `<${prop.tag}`
            + `${prop.lang ? ` lang="${prop.lang}"` : ''}`
            + `${prop.scoped ? ' scoped' : ''}>\r\n`
            + `${content}\r\n</${prop.tag}>\r\n`
        }))
        .pipe(rename(function (file) { file.extname = '.vue' }))
        .pipe(dest(destination || this.destination, {append: true}))
        .on('end', res)
    })
  }
  
  public cleanDir = (dirPath: string): Promise<void> => {
    return new Promise(res => {
      src(dirPath, {allowEmpty: true})
        .pipe(clean())
        .on('error', loganize.error)
        // fix end emit, listen the data
        .on('data', function () {})
        .on('end', res)
    })
  }
  
  public build = (cb) => {
    loganize.info(`Cleaning ...`)
    this.cleanDir(this.destination)
      .then(() => {
        loganize.info(`Cleaned`)
        loganize.info(`Building ...`)
        Promise.all(
          this.toProcess.map(x => this.dataChunker(`${x.dir}/**/*.${x.prop.ext}`, x.prop))
        ).then(() => {
          loganize.info(`Builded`)
          cb()
        })
      })
  }

  public watcher = () => {
    const pathsRegExp = this.toProcess
      .map(tp => new RegExp(`^${tp.dir.replace('./', '')}/([^.]*)\.(.*)$`))
      
    watch(this.toProcess.map(tp => `${tp.dir.replace('./', '')}/**/*.${tp.prop.ext}`))
      .on('change', (changedFile) => {
        loganize.info(`Updating ${changedFile}`)

        const relativePath = pathsRegExp
          .filter(regexp => regexp.test(changedFile))
          .map(regexp => changedFile.match(regexp)[1])[0]

        const destination = `${this.destination}/${relativePath}.vue`
        
        this.cleanDir(destination)
          .then(() => {
            this.toProcess
              .map(async item => {
                const dir = `${item.dir}/${relativePath}.${item.prop.ext}`
                return await this.dataChunker(dir, item.prop, destination.replace(/\/[^\/]*$/, ''))
              })
            loganize.info(`Updated ${changedFile}`)
          })
      })
  }

  public exporters() {
    return {
      default: series(this.build, this.watcher),
      build: this.build,
      watch: this.watcher,
    }
  }
}

const gvb = new GulpVueBuilder(tp)

exports.default = gvb.exporters().default