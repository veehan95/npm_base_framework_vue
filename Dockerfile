FROM node:12

WORKDIR /app

RUN npm i -g yarn
COPY ./package.json .
RUN yarn install

COPY . .

EXPOSE 8080/tcp

CMD ["yarn", "serve"]
