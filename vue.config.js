const path = require('path')

module.exports = {
  configureWebpack: {
    resolve: {
      alias: {
        '@': path.join(__dirname, 'build/'),
        '@assets': path.join(__dirname, 'src/assets/'),
        '@router': path.join(__dirname, 'src/router/'),
      }
    }
  }
}